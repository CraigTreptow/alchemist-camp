type = IO.gets(~s{"Would you like count "lines", "words", or "characters"?\n})

regexp =
  case String.trim(type) do
    "lines" ->
      ~r{\n}

    "words" ->
      ~r{(\\n|[^\w'])+}

    "characters" ->
      ""

    _ ->
      IO.puts(~s{Type "lines", "words", or "characters" only, please.})
  end

filename = IO.gets("File to count words in: ") |> String.trim()
not_empty = fn x -> x != "" end

words =
  File.read!(filename)
  |> String.split(regexp)
  |> Enum.filter(not_empty)

IO.inspect(words)

words
|> Enum.count()
|> IO.puts()
