defmodule GuessingName do
  def guess do
    answer = IO.gets("Hey, what's your name?\n")

    case String.trim(answer) |> String.downcase() do
      "craig" -> IO.puts("Yippee!  Craig programmed me!")
      _ -> IO.puts("That's a nice name.")
    end
  end
end
